# Maintainer: Perttu Luukko <perttu.luukko@iki.fi>
# Forked from vtk-py3-qt by Alex Maystrenko <alexeytech@gmail.com>
# Itself forked from the main Arch Linux vtk package

pkgname=vtk-mayavi-minimal
pkgver=7.0.0
_majorver=7.0
pkgrel=3
pkgdesc='Minimal VTK package for use with mayavi-py3'
arch=('i686' 'x86_64')
url='http://www.vtk.org/'
license=('BSD')
conflicts=('vtk' 'vtk-py3-qt4')
provides=('vtk-py3-qt4')
depends=('qt4' 'python' 'jsoncpp')
makedepends=('boost' 'cmake' 'ninja')
install=vtk-mayavi-minimal.install

source=("http://www.vtk.org/files/release/${_majorver}/VTK-${pkgver}.tar.gz"
        gdal2.patch
        ffmpeg3_compat.patch
        remove_args.patch
        find_gcc6.patch
        fix-possible-segfault.patch
        fix-qt4-moc.patch)
options=(staticlibs !strip)
sha1sums=('7719fac36b36965eaf5076542166ba49bbe7afbb'
          'c60610e7c8cf0ad93d7c02cbf8a20fc415f59b3e'
          'a78177f8dd6dedd9ad189fa12730ec53c7d02508'
          'd880f0f7d979cc9efa9246eda82880fef29172c2'
          '127d302f46e2691980af83350542243ab76ca75f'
          '1b04eadd40b823092d620914ce5bc8314e1e4980'
          '9b1523da7b111a02390e85a5eee2b015680d05ea')

prepare() {
  cd "${srcdir}"/VTK-$pkgver

  patch -p1 < ../ffmpeg3_compat.patch # http://www.vtk.org/Bug/view.php?id=16001
  patch -p1 < ../gdal2.patch # https://github.com/Kitware/VTK/pull/21
  patch -p1 < ../remove_args.patch # https://gitlab.kitware.com/vtk/vtk/commit/52501fd085a64b55d1b53d40c1d3f86c6ce9addd
  patch -p1 < ../find_gcc6.patch # https://gitlab.kitware.com/vtk/vtk/commit/06e2a00bf8accb04db63b3c1c7a454e4afc6fea6
  patch -p1 < ../fix-possible-segfault.patch
  patch -p1 < ../fix-qt4-moc.patch
}

build() {
  cd "${srcdir}"
  test -d build && msg2 "note: using existing build directory"
  mkdir -p build
  cd build

  # flags to enable using system libs
  local cmake_system_flags=""
  # TODO: try to use system provided XDMF2, XDMF3, LIBPROJ4 NETCDF
  # VTK fails to compile with recent netcdf-cxx package, VTK should be ported to the latest API
  # VTK does not work with XDMF2 compiled from git. TODO: make vtk compatible with system XDMF library.
  # Note: VTK explicitly disables system GLEW dependency, it uses embedded sources with modifications
  for lib in EXPAT FREETYPE JPEG PNG TIFF ZLIB LIBXML2 OGGTHEORA TWISTED ZOPE SIX AUTOBAHN MPI4PY JSONCPP GLEW GL2PS HDF5 SQLITE; do
    cmake_system_flags+="-DVTK_USE_SYSTEM_${lib}:BOOL=ON "
  done

  cmake \
    -Wno-dev \
    -DCMAKE_SKIP_RPATH=ON \
    -DBUILD_SHARED_LIBS:BOOL=ON \
    -DCMAKE_INSTALL_PREFIX:FILEPATH=/usr \
    -DBUILD_DOCUMENTATION:BOOL=OFF \
    -DBUILD_EXAMPLES:BOOL=OFF \
    -DVTK_USE_FFMPEG_ENCODER:BOOL=ON \
    -DVTK_BUILD_ALL_MODULES:BOOL=OFF \
    -DVTK_Group_Imaging:BOOL=OFF \
    -DVTK_Group_MPI:BOOL=OFF \
    -DVTK_Group_Qt:BOOL=OFF \
    -DVTK_Group_Rendering:BOOL=OFF \
    -DVTK_Group_StandAlone:BOOL=OFF \
    -DVTK_Group_Tk:BOOL=OFF \
    -DVTK_Group_Views:BOOL=OFF \
    -DVTK_Group_Web:BOOL=OFF \
    -DModule_vtkGUISupportQt=ON \
    -DModule_vtkCommonComputationalGeometry=ON \
    -DModule_vtkCommonCore=ON \
    -DModule_vtkCommonDataModel=ON \
    -DModule_vtkCommonExecutionModel=ON \
    -DModule_vtkCommonTransforms=ON \
    -DModule_vtkFiltersCore=ON \
    -DModule_vtkFiltersExtraction=ON \
    -DModule_vtkFiltersFlowPaths=ON \
    -DModule_vtkFiltersGeneral=ON \
    -DModule_vtkFiltersGeometry=ON \
    -DModule_vtkFiltersHybrid=ON \
    -DModule_vtkFiltersModeling=ON \
    -DModule_vtkFiltersSources=ON \
    -DModule_vtkFiltersTexture=ON \
    -DModule_vtkIOExodus=ON \
    -DModule_vtkIOExport=ON \
    -DModule_vtkIOGeometry=ON \
    -DModule_vtkIOImage=ON \
    -DModule_vtkIOImport=ON \
    -DModule_vtkIOLegacy=ON \
    -DModule_vtkIOMINC=ON \
    -DModule_vtkIOPLY=ON \
    -DModule_vtkIOParallel=ON \
    -DModule_vtkIOXML=ON \
    -DModule_vtkImagingCore=ON \
    -DModule_vtkImagingGeneral=ON \
    -DModule_vtkImagingHybrid=ON \
    -DModule_vtkImagingSources=ON \
    -DModule_vtkInteractionStyle=ON \
    -DModule_vtkInteractionWidgets=ON \
    -DModule_vtkRenderingAnnotation=ON \
    -DModule_vtkRenderingCore=ON \
    -DModule_vtkRenderingFreeType=ON \
    -DModule_vtkRenderingLabel=ON \
    -DModule_vtkRenderingVolume=ON \
    -DVTK_USE_LARGE_DATA:BOOL=OFF \
    -DVTK_QT_VERSION:STRING="4" \
    -DVTK_WRAP_JAVA:BOOL=OFF \
    -DVTK_WRAP_PYTHON:BOOL=ON \
    -DVTK_PYTHON_VERSION="3" \
    -DVTK_WRAP_TCL:BOOL=OFF \
    -DCMAKE_CXX_FLAGS="-D__STDC_CONSTANT_MACROS" \
    -DVTK_CUSTOM_LIBRARY_SUFFIX="" \
    -DVTK_INSTALL_INCLUDE_DIR:PATH=include/vtk \
    -DBUILD_TESTING:BOOL=OFF \
    ${cmake_system_flags} \
    -DCMAKE_BUILD_TYPE=Release \
    "${srcdir}/VTK-$pkgver" \
    -GNinja

  ninja
}

package() {
  cd "${srcdir}/build"

  DESTDIR="${pkgdir}" ninja install

  # Install license
  install -dv "${pkgdir}/usr/share/licenses/vtk"
  install -m644 "${srcdir}/VTK-$pkgver/Copyright.txt" "${pkgdir}/usr/share/licenses/vtk"
  # Remove QT designer plugin
  rm -rf "${pkgdir}/usr/plugins"
}
